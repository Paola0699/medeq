DROP DATABASE IF EXISTS medeq;
CREATE DATABASE medeq;
USE medeq;

CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE PRODUCTO(
  id INT NOT NULL AUTO_INCREMENT,
  clave BIGINT UNIQUE,
  nombre VARCHAR(150),
  descripcion VARCHAR(5000),
  unidadmedida INT,
  presentacion VARCHAR(5),
  marcafabricante VARCHAR(100),
  cantidad INT,
  preciounitario FLOAT,
  importe FLOAT,
  importetotal FLOAT,
  PRIMARY KEY(id)
);

CREATE TABLE HOSPITAL(
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(150),
  direccion VARCHAR(500),
  doctorencargado VARCHAR(250),
  PRIMARY KEY(id)
);

CREATE TABLE MEDICO(
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(150),
  hospital INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(hospital) REFERENCES HOSPITAL(id) ON DELETE RESTRICT
 );

CREATE TABLE CIRUGIA(
  id INT NOT NULL AUTO_INCREMENT,
  fecha DATE,
  folio INT,
  hora TIME,
  paciente VARCHAR(45),
  registro VARCHAR(45),
  cx VARCHAR(250),
  rem VARCHAR(45),
  asiste VARCHAR(45),
  marca VARCHAR(45),
  hospital INT NOT NULL,
  medico int NOT NULL,
  status bool DEFAULT 0,
  PRIMARY KEY(id),
  FOREIGN KEY(hospital) REFERENCES HOSPITAL(id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY(medico) REFERENCES MEDICO(id) ON DELETE RESTRICT ON UPDATE CASCADE
 );

CREATE TABLE PRODUCTOCIRUGIA(
  producto INT NOT NULL,
  cirugia INT NOT NULL,
  cantidad INT,
  precio FLOAT,
  PRIMARY KEY(producto,cirugia),
  FOREIGN KEY(producto) REFERENCES PRODUCTO(id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY(cirugia) REFERENCES CIRUGIA(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE CONTRATO(
  id int NOT null AUTO_INCREMENT,
  numero VARCHAR(25),
  PRIMARY KEY(id)
);
INSERT INTO HOSPITAL(nombre,direccion,doctorencargado) VALUES('HOSPITAL METROPOLITANO','AV. ADOLFO LOPEZ MATEOS No.4600 COL. BOSQUES DEL NOGALAR CP.66480 SAN NICOLAS, NUEVO LEON', 'DR. BERNARDO SEPULVEDA');
delimiter $$
CREATE TRIGGER act_importe 
BEFORE INSERT ON PRODUCTO 
FOR EACH ROW
BEGIN
	set new.importe = new.preciounitario*1.16;
  set new.importetotal = new.cantidad * new.importe;
END $$

CREATE TRIGGER act_importe_up 
BEFORE UPDATE ON PRODUCTO 
FOR EACH ROW
BEGIN
  set new.importe = new.preciounitario*1.16;
  set new.importetotal = new.cantidad * new.importe;
END$$