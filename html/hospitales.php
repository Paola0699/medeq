<?php
    // Initialize the session
    session_start();
     
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: ../html/login.php");
        exit;
    }
    include_once "base_de_datos.php";
    $sentencia = $base_de_datos->query("SELECT * FROM HOSPITAL");
    $hospitales= $sentencia->fetchAll(PDO::FETCH_OBJ);
    $sentencia2 = $base_de_datos->query("SELECT count(*) as total FROM HOSPITAL");
    $totales= $sentencia2->fetchAll(PDO::FETCH_OBJ);
    $sentencia3 = $base_de_datos->query("SELECT count(*) as total FROM MEDICO");
    $totales2 = $sentencia3->fetchAll(PDO::FETCH_OBJ);
    $sentencia4 = $base_de_datos->query("SELECT count(*) as total FROM producto");
    $totales3 = $sentencia4->fetchAll(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/logo_2.jpg">
    <title>MEDEQ</title>
    <!-- This page plugin CSS -->
    <link href="../assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../dist/css/style.min.css" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <div class="navbar-brand">
                        <a href="index.php">
                            <span class="logo-text">
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" alt="homepage" class="dark-logo" />
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1"> 
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/images/users/1.jpg" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>Hola,</span> <span
                                        class="text-dark"><?php echo htmlspecialchars($_SESSION["username"]); ?></span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="logout.php"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link" href="index.php"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Inicio</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Nuevo</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="hospitales.php"
                                aria-expanded="false"><i class="feather-icon fas fa-hospital"></i><span
                                    class="hide-menu">Hospital
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="doctores.php"
                                aria-expanded="false"><i class="feather-icon  fas fa-user"></i><span
                                    class="hide-menu">Doctor</span></a></li>
                        <li class="sidebar-item"> <a  class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon fab fa-product-hunt"></i><span
                                    class="hide-menu">Productos </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nuevo_producto.php" class="sidebar-link"><span class="hide-menu"> Nuevo Producto </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_productos.php" class="sidebar-link"><span class="hide-menu"> Lista Productos</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Generar</span></li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon  fas fa-medkit"></i><span
                                    class="hide-menu">Cirugías
                                </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nueva_cirugia.php" class="sidebar-link"><span class="hide-menu"> Nueva Cirugía
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_cirugias.php" class="sidebar-link"><span class="hide-menu">Lista Cirugías
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="almacen.php"
                                aria-expanded="false"><i  class="feather-icon  fas fa-window-restore"></i><span
                                    class="hide-menu">Almacén
                                </span></a>
                        </li>
                        <li class="list-divider"></li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="logout.php"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Cerrar sesión</span></a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Hospitales</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.php" class="text-muted">Inicio</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Hospitales</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <?php 
                    if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                    }
                ?>

                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-hover">
                            <div class="p-2 bg-primary text-center">
                                <?php foreach($totales as $total){ ?>
                                    <h1 class="font-light text-white"><?php echo $total->total?></h1>
                                <?php } ?>
                                <h6 class="text-white">Hospitales registrados</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-hover">
                            <div class="p-2 bg-cyan text-center">
                                <?php foreach($totales2 as $total){ ?>
                                    <h1 class="font-light text-white"><?php echo $total->total?></h1>
                                <?php } ?>
                                <h6 class="text-white">Doctores Registrados</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-hover">
                            <div class="p-2 bg-success text-center">
                                <?php foreach($totales3 as $total){ ?>
                                    <h1 class="font-light text-white"><?php echo $total->total?></h1>
                                <?php } ?>
                                <h6 class="text-white">Productos Registrados</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="row">
                    <div class="col-12">
                         <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Nuevo Hospital</h4>
                                <form role="form" name="registro" action="../php/nuevo.php" method="post">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="text" class="form-control" placeholder="Nombre" name="nombre" required="">
                                        </div>
                                        <div class="col-3">
                                              <input type="text" class="form-control" placeholder="Doctor Titular" name="doctorencargado" required="">
                                        </div>
                                        <div class="col-3">
                                              <textarea required="" class="form-control" rows="1" placeholder="Dirección" name="direccion" ></textarea>
                                        </div>
                                        <div class="col-3">
                                            <input type="submit" name="hospital" value="Agregar Hospital" class="btn btn-block btn-primary">
                                        </div>
                                    </div>    
                                </form>
                                <br>
                                 <div class="table-responsive">
                                    <table id="multi_col_order"
                                        class="table table-striped table-bordered display" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nombre</th>
                                                <th scope="col">Titular</th>
                                                <th scope="col">Dirección</th>
                                                <th scope="col">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($hospitales as $hospital) {?>
                                                <tr>
                                                    <th scope="row"><?php echo $hospital->id ?></th>
                                                <td><?php echo $hospital->nombre ?></td>
                                                <td><?php echo $hospital->doctorencargado ?></td>
                                                <td><?php echo $hospital->direccion ?></td>
                                                <td>
                                                    <form method="post" action="../php/nuevo.php" >
                                                            <input type="hidden" name="id" value="<?php echo $hospital->id ?>">
                                                            <button style="background-color: transparent; color: #6c757d; border-color: transparent;" type="submit" name="eliminarHospital" class="btn btn-secondary btn-circle"  data-toggle="tooltip" title="Eliminar"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </td>
                                                </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="../dist/js/app-style-switcher.js"></script>
    <script src="../dist/js/feather.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <!-- themejs -->
    <!--Menu sidebar -->
    <script src="../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../dist/js/custom.min.js"></script>
    <!--This page plugins -->
    <script src="../assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../dist/js/pages/datatable/datatable-basic.init.js"></script>
</body>

</html>