<?php
session_start();
 if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../html/login.php");
    exit;
}
    include_once "base_de_datos.php";
    $sentencia = $base_de_datos->query("SELECT cx, COUNT(*) as total FROM cirugia GROUP BY cx ORDER BY total DESC LIMIT 5");
    $cirugias= $sentencia->fetchAll(PDO::FETCH_OBJ);
    $contador = 1;
    $sentencia2 = $base_de_datos->query("SELECT COUNT(*) as total FROM cirugia where status=0");
    $totalcirugias= $sentencia2->fetchAll(PDO::FETCH_OBJ);
    $sentencia3 = $base_de_datos->query("SELECT COUNT(*) as total FROM cirugia where status=1");
    $totalcanceladas= $sentencia3->fetchAll(PDO::FETCH_OBJ);
    $total=0;
    $canceladas=0;
    foreach ($totalcirugias as $totalcirugia) {
        $total += $totalcirugia->total;
    }
    foreach ($totalcanceladas as $totalcancelada) {
        $canceladas += $totalcancelada->total;
    }
    $sentencia4 = $base_de_datos->query("SELECT p.nombre, COUNT(pc.producto) as cant from productocirugia pc join producto p on pc.producto = p.id GROUP BY pc.producto ORDER BY cant DESC LIMIT 5");
    $productos= $sentencia4->fetchAll(PDO::FETCH_OBJ);
    $sentencia5 = $base_de_datos->query("SELECT * FROM contrato order by id desc limit 1");
    $contratos= $sentencia5->fetchAll(PDO::FETCH_OBJ);
    $numerocontrato =0;
    foreach($contratos as $contrato){
        $numerocontrato = $contrato->numero;
    }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/logo_2.jpg">
    <title>MEDEQ</title>
    <link href="../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="../dist/css/style.min.css" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <div class="navbar-brand">
                        <a href="index.php">
                            <span class="logo-text">
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" alt="homepage" class="dark-logo" />
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1"> 
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/images/users/1.jpg" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>Hola,</span> <span
                                        class="text-dark"><?php echo htmlspecialchars($_SESSION["username"]); ?></span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="logout.php"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link" href="index.php"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Inicio</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Nuevo</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="hospitales.php"
                                aria-expanded="false"><i class="feather-icon fas fa-hospital"></i><span
                                    class="hide-menu">Hospital
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="doctores.php"
                                aria-expanded="false"><i class="feather-icon  fas fa-user"></i><span
                                    class="hide-menu">Doctor</span></a></li>
                        <li class="sidebar-item"> <a  class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon fab fa-product-hunt"></i><span
                                    class="hide-menu">Productos </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nuevo_producto.php" class="sidebar-link"><span class="hide-menu"> Nuevo Producto </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_productos.php" class="sidebar-link"><span class="hide-menu"> Lista Productos</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Generar</span></li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon  fas fa-medkit"></i><span
                                    class="hide-menu">Cirugías
                                </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nueva_cirugia.php" class="sidebar-link"><span class="hide-menu"> Nueva Cirugía
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_cirugias.php" class="sidebar-link"><span class="hide-menu">Lista Cirugías
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="almacen.php"
                                aria-expanded="false"><i  class="feather-icon  fas fa-window-restore"></i><span
                                    class="hide-menu">Almacén
                                </span></a>
                        </li>
                        <li class="list-divider"></li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="logout.php"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Cerrar sesión</span></a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Buenos días <?php echo htmlspecialchars($_SESSION["username"]); ?>!</h3>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.php">Dashboard</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-5 align-self-center">
                        
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <?php 
                    if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                    }
                ?>
                <div class="card-group">
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium"><?php echo number_format($total)?></h2> 
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Cirugías cubiertas   </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i class="fas fa-capsules" style="font-size: 1.5em"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><?php echo number_format($canceladas)?></h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Cirugías canceladas
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i class=" fas fa-times" style="font-size: 1.5em"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h3 class="text-dark mb-1 font-weight-medium"><?php echo $numerocontrato ?></h3>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Número de contrato actual</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i data-feather="file-plus"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form role="form" name="registro" action="../php/nuevo7.php" method="post">
                                <div class="d-flex d-lg-flex d-md-block align-items-center">
                                    <div>
                                        Actualizar # de contrato:
                                        <input type="text" name="numero">
                                    </div>
                                    <div class="ml-auto mt-md-3 mt-lg-0">
                                        <button type="submit" class="btn btn-warning btn-circle"><i class="fa fa-check"></i>
                                    </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Costo de Cirugías</h4>
                                <form method="post" action="index.php">    
                                    <div class="row">
                                        <div class="col-5">
                                            <input type="date" name="inicio" class="form-control">
                                        </div>
                                        <div class="col-5">
                                            <input type="date" name="fin" class="form-control">
                                        </div>
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-warning btn-circle" name="costoCirugia"><i class="fa fa-check"></i>
                                            </button>
                                        </div> 
                                    </div>
                                </form>
                                <br>
                                 <?php
                                    if(isset($_POST["costoCirugia"])){
                                        if(isset($_POST["inicio"]) && isset($_POST["fin"])){
                                            $inicio=$_POST["inicio"];
                                            $fin=$_POST["fin"];
                                            $sentencia6 = $base_de_datos->query("SELECT sum(pc.precio*pc.cantidad) as suma FROM productocirugia pc join cirugia c on pc.cirugia = c.id WHERE c.fecha>='$inicio' AND c.fecha<='$fin'");
                                            $costos= $sentencia6->fetchAll(PDO::FETCH_OBJ);
                                            $costito=0;
                                            foreach($costos as $costo){ 
                                                $costito +=$costo->suma;
                                            }
                                            echo "<p style='text-align: center'>Costo de cirugías del ". date("d/m/Y", strtotime($inicio)). " al " . date("d/m/Y", strtotime($fin))."</p>";
                                            echo "<h1 style='text-align: center; font-size:3em; color:#ff8f00 '>$".number_format($costito,2)."</h1";
                                        }
                                    }
                                ?>

                                <h4 class="card-title">Venta de cirugías sin IVA</h4>
                                <form method="post" action="index.php">
                                     <div class="row">
                                        <div class="col-5">
                                            <input type="date" name="inicio2" class="form-control">
                                        </div>
                                        <div class="col-5">
                                            <input type="date" name="fin2" class="form-control">
                                        </div>
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-warning btn-circle" name="ventaCirugia"><i class="fa fa-check"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <br>
                                <?php
                                    if(isset($_POST["ventaCirugia"])){
                                        if(isset($_POST["inicio2"]) && isset($_POST["fin2"])){
                                            $inicio2=$_POST["inicio2"];
                                            $fin2=$_POST["fin2"];
                                            $sentencia7 = $base_de_datos->query("SELECT sum(p.preciounitario*pc.cantidad) as suma FROM productocirugia pc join cirugia c on pc.cirugia = c.id join producto p on pc.producto=p.id WHERE c.fecha>='$inicio2' AND c.fecha<='$fin2'");
                                            $ventas= $sentencia7->fetchAll(PDO::FETCH_OBJ);
                                            $ventita=0;
                                            foreach($ventas as $venta){ 
                                                $ventita +=$venta->suma;
                                            }
                                            echo "<p style='text-align: center'>Venta de cirugías sin IVA del ". date("d/m/Y", strtotime($inicio2)). " al " . date("d/m/Y", strtotime($fin2))."</p>";
                                            echo "<h1 style='text-align: center; font-size:3em; color:#ff8f00 '>$".number_format($ventita,2)."</h1";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-6 col-lg-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Cirugías más realizadas</h4>
                                <div class="mt-4 activity">
                                    <?php foreach($cirugias as $cirugia){ ?>
                                    <div class="d-flex align-items-start border-left-line pb-3">
                                        <div>
                                            <a href="javascript:void(0)" class="btn btn-info btn-circle mb-2 btn-item">
                                                <?php echo $contador; $contador++?>
                                            </a>
                                        </div>
                                        <div class="ml-3 mt-2">
                                            <h5 class="text-dark font-weight-medium mb-2"><?php echo $cirugia->cx?></h5>
                                            <p class="font-14 mb-2 text-muted">Se han realizado <b><?php echo $cirugia->total?></b> cirugías como esta.
                                            </p>
                                        </div>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-7 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-4">Productos más vendidos</h4>
                                <?php foreach($productos as $producto){ $porcentaje= number_format($producto->cant / $total *100,0);?>
                                    <div class="row mb-3 align-items-center mt-1 mt-5">
                                        <div class="col-4 text-right">
                                            <span class="text-muted font-14" style="color: #1c2d41!important"><?php echo $producto->nombre?></span>
                                        </div>
                                        <div class="col-5">
                                            <div class="progress progress-xl mb-2">
                                                <div class="progress-bar bg-primary progress-xl mb-2" role="progressbar" <?php echo "style='width:".$porcentaje."%'";?>
                                                    aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-3 text-left">
                                            <span class="mb-0 font-14 text-dark font-weight-medium"><?php echo $porcentaje ."%"?></span>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../dist/js/app-style-switcher.js"></script>
    <script src="../dist/js/feather.min.js"></script>
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../dist/js/sidebarmenu.js"></script>
    <script src="../dist/js/custom.min.js"></script>
    <script src="../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../assets/extra-libs/c3/c3.min.js"></script>
    <script src="../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="../assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="../assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    <script src="../dist/js/pages/dashboards/dashboard1.min.js"></script>
</body>

</html>