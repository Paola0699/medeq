<?php ob_start(); ?>
<?php
		$id = $_GET["id"];
        include_once "base_de_datos.php";
        $sentencia = $base_de_datos->query("SELECT c.*,  p.clave as clave, p.descripcion as descripcion, p.unidadmedida as unidadmedida, p.preciounitario as unitario  FROM productocirugia c join producto p on c.producto = p.id  where c.cirugia=$id");
        $productos= $sentencia->fetchAll(PDO::FETCH_OBJ);
        $sentencia2 = $base_de_datos->prepare("SELECT c.*, h.nombre as unidad, h.direccion as direccion, h.doctorencargado as encargado from cirugia c join hospital h on c.hospital=h.id WHERE c.id=?;");
    	$sentencia2->execute([$id]);
        $cirugia = $sentencia2 -> fetch(PDO::FETCH_OBJ);
        $total = 0;
        $sentencia5 = $base_de_datos->query("SELECT * FROM contrato order by id desc limit 1");
	    $contratos= $sentencia5->fetchAll(PDO::FETCH_OBJ);
	    $numerocontrato =0;
	    foreach($contratos as $contrato){
	        $numerocontrato = $contrato->numero;
	    }
?>
<style type="text/css">
	table, th, td {
  		border: 1px solid black;
  		border-collapse: collapse;
  		font-size: .8em;
  		text-align: center;
	}
	@page{
		font-family: Arial, Helvetica, sans-serif;
	}
</style>
<!DOCTYPE html>
<html>
<body>
	<div style="width: 100%">
		<div style="width: 100%">
			<div style="width:70%; display: inline-block;">
				<img src="logo_1.png" style="width: 100%">
			</div>
			<div style="width: 18%; display: inline-block; height:110px; font-size: 1.3em; margin-left: 5%">
				<b>REMISIÓN
				<br> No. <?php echo $cirugia->rem ?></b>
			</div>
		</div>
		<br>
		<div style="width: 100%; margin-top: 2%">
				<div style="width: 37%; height: 80px; padding: 1.5%; border: 1px solid black; font-size: .8em; display: inline-block;">
					<b>CLIENTE 447 RFC SSN970115QI9<br>
						SERVICIOS DE SALUD DE NUEVO LEÓN <br>
						MATAMOROS OTE. 520 CENTRO                         
						<br>CP 64000
						<br>MONTERREY NUEVO LEÓN
					</b>
				</div>
				<div style="width: 40%; height: 80px; margin-left:14%; padding: 1.5%; border: 1px solid black; font-size: .8em; display: inline-block;">
					<b>
						<?php echo $cirugia->unidad ."<br>". $cirugia->encargado ."<br>". $cirugia->direccion?>
					</b>
				</div>	
		</div>
		<div style="width: 100%; font-size: .75em">
			<p>"EN LA PRESENTE REMISIÓN DIRIGIDA AL DEPARTAMENTO DE ORTOPEDIA Y TRAUMATOLOGIA SE DESCRIBEN LOS INSUMOS Y EQUIPOS ENTREGADOS Y CONSUMIDOS EN EL PROCEDIMIENTO AL PACIENTE QUE SE MENCIONA."</p>
		</div>
	</div>
	<table>
		<tr>
			<th>RENGLÓN</th>
			<th>CLAVE</th>
			<th>DESCRIPCIÓN</th>
			<th>UNIDAD DE MEDIDA</th>
			<th>CANTIDAD</th>
			<th>PRECIO UNITARIO</th>
			<th>IMPORTE</th>
			<th>IMPORTE TOTAL</th>
		</tr>
		<?php foreach($productos as $producto){ ?>
			<tr>
				<td><?php echo $producto->producto?></td>
				<td><?php echo $producto->clave?></td>
				<td style="text-align: justify!important;"><?php echo $producto->descripcion?></td>
				<td>
					<?php 
							if($producto->unidadmedida==1){
								echo "ROLLO";
							}elseif ($producto->unidadmedida==2) {
								echo "PIEZA";
							}elseif ($producto->unidadmedida== 3) {
								echo "SET";
							}elseif ($producto->unidadmedida==4) {
								echo "SISTEMA";
							}elseif ($producto->unidadmedida==5) {
								echo "EQUIPO";
							}
					?>	
				</td>
				<td><?php echo $producto->cantidad?></td>
				<td><?php echo "$ ".number_format($producto->unitario,2);?></td>
				<td><?php echo "$" . number_format($producto->cantidad * $producto->unitario,2)?></td>
				<td><?php echo "$" . number_format($producto->cantidad * $producto->unitario,2)?></td>
				<?php $total= $total + ($producto->cantidad * $producto->unitario)  ?>
			</tr>			
		<?php } ?>
			<tr style="border:solid 0px">
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td>SUB-TOTAL</td>
				<td><?php echo "$". number_format($total,2)?></td>
			</tr>
			<tr style="border:solid 0px">
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td>IVA</td>
				<td><?php echo "$". number_format($total*.16,2)?></td>
			</tr>
			<tr style="border:solid 0px">
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td style="border: solid 0px"></td>
				<td><b>TOTAL</b></td>
				<td><?php echo "<b>$". number_format($total*1.16,2)."</b>"?></td>
			</tr>
	</table>
	<div style="width: 60%; font-size: .7em; margin-top: 2%">
		FECHA DE CIRUGÍA: <?php echo date("d/m/Y", strtotime($cirugia->fecha)) ?>
		<br>
		NUMERO DE CONTRATO: <?php echo $numerocontrato ?>
		<br>
		NOMBRE DE LA UNIDAD MÉDICA: <?php echo $cirugia->unidad?>
		<br>
		NOMBRE DEL PACIENTE: <?php echo $cirugia->paciente?>
	</div>
	<?php if($cirugia->hospital == 1){?>
		<img src="firma_1.jpg" style="width: 100%; margin-top: 10%">
	<?php } ?>
</body>
</html>


<?php
	require_once 'dompdf/autoload.inc.php';
	use Dompdf\Dompdf;
	$dompdf = new DOMPDF();
	$dompdf->load_html(ob_get_clean());
	$dompdf->setPaper('A4', 'portrait');
	$dompdf->render();
	$pdf = $dompdf->output();
	$filename = "remision_$cirugia->rem.pdf";
	file_put_contents($filename, $pdf);
	$dompdf->stream($filename);
?>
