<?php
  session_start();
     if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: ../html/login.php");
        exit;
    }
    if (!isset($_GET["id"])) exit();
    $id = $_GET["id"];
    include_once "base_de_datos.php";
    $sentencia = $base_de_datos->prepare("SELECT * FROM cirugia WHERE id=?;");
    $sentencia->execute([$id]);
    $cirugia = $sentencia -> fetch(PDO::FETCH_OBJ);
    if($cirugia === FALSE){
    	echo "No existe esa cirugia";
    	exit();
    }
    $sentencia2 = $base_de_datos->query("SELECT * FROM PRODUCTO");
    $productos= $sentencia2->fetchAll(PDO::FETCH_OBJ);
    $sentencia3 = $base_de_datos->query("SELECT p.id as id, p.nombre as nombre, p.preciounitario as unitario, c.precio as precio, c.cantidad as cantidad FROM PRODUCTOCIRUGIA c join PRODUCTO p on c.producto = p.id where c.cirugia = $id");
    $consumos= $sentencia3->fetchAll(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/logo_2.jpg">
    <title>MEDEQ</title>
    <!-- This page plugin CSS -->
    <link href="../assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom CSS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="functions.js"></script>
    <link href="../dist/css/style.min.css" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <div class="navbar-brand">
                        <a href="index.php">
                            <span class="logo-text">
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" alt="homepage" class="dark-logo" />
                                <img style= "width: 100%" src="../assets/images/logo_1.jpg" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1"> 
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/images/users/1.jpg" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>Hola,</span> <span
                                        class="text-dark"><?php echo htmlspecialchars($_SESSION["username"]); ?></span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="logout.php"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link" href="index.php"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Inicio</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Nuevo</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="hospitales.php"
                                aria-expanded="false"><i class="feather-icon fas fa-hospital"></i><span
                                    class="hide-menu">Hospital
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="doctores.php"
                                aria-expanded="false"><i class="feather-icon  fas fa-user"></i><span
                                    class="hide-menu">Doctor</span></a></li>
                        <li class="sidebar-item"> <a  class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon fab fa-product-hunt"></i><span
                                    class="hide-menu">Productos </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nuevo_producto.php" class="sidebar-link"><span class="hide-menu"> Nuevo Producto </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_productos.php" class="sidebar-link"><span class="hide-menu"> Lista Productos</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Generar</span></li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="feather-icon  fas fa-medkit"></i><span
                                    class="hide-menu">Cirugías
                                </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="nueva_cirugia.php" class="sidebar-link"><span class="hide-menu"> Nueva Cirugía
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="lista_cirugias.php" class="sidebar-link"><span class="hide-menu">Lista Cirugías
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="almacen.php"
                                aria-expanded="false"><i  class="feather-icon  fas fa-window-restore"></i><span
                                    class="hide-menu">Almacén
                                </span></a>
                        </li>
                        <li class="list-divider"></li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="logout.php"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Cerrar sesión</span></a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Cirugías</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.php" class="text-muted">Inicio</a></li>
                                    <li class="breadcrumb-item"><a href="lista_cirugias.php" class="text-muted">Lista Cirugías</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Editar Cirugía</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                  <?php 
                    if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $_SESSION['message']; ?>
                    </div>
                <?php

                    unset($_SESSION['message']);
                    }
                ?>
                <!-- multi-column ordering -->
                <div class="row">
                    <div class="col-7">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Editar Cirugía</h4>
                                <form role="form" name="registro" action="../php/nuevo5.php" method="post">
                                    <div class="form-body">
                                        <label>Datos Generales </label>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="hidden" name="id" value="<?php echo $cirugia->id ?>">
                                                <div class="form-group">
                                                    <b>Fecha:</b>
                                                    <input type="date" class="form-control" name="fecha" value="<?php echo $cirugia->fecha ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Folio:</b>
                                                    <input type="number" class="form-control" placeholder="Folio" name="folio" value="<?php echo $cirugia->folio ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Registro:</b>
                                                    <input type="text" class="form-control" placeholder="Registro" name="registro" value="<?php echo $cirugia->registro ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Remisión:</b>
                                                    <input type="text" class="form-control" placeholder="Remisión" name="remision" value="<?php echo $cirugia->rem ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                 <div class="form-group">
                                                    <b>Hora:</b>
                                                    <input type="time" class="form-control" name="hora" value="<?php echo $cirugia->hora ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Paciente:</b>
                                                    <input type="text" class="form-control" placeholder="Paciente" name="paciente" value="<?php echo $cirugia->paciente ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Cx:</b>
                                                    <input type="text" class="form-control" placeholder="Cx" name="cx" value="<?php echo $cirugia->cx ?>">
                                                </div>
                                                <div class="form-group">
                                                    <b>Asiste:</b>
                                                    <input type="text" class="form-control" placeholder="Asiste" name="asiste" value="<?php echo $cirugia->asiste ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <label>Datos complementarios</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <b>Marca:</b>
                                                    <input type="text" class="form-control" placeholder="Marca" name="marca" value="<?php echo $cirugia->marca ?>">
                                                </div>
                                                <input type="hidden" class="custom-control-input" value="0" name="status2">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2" value="1" name="status"
                                                    <?php
                                                        if ($cirugia->status == 1) {
                                                                echo "checked";
                                                        }
                                                    ?>
                                                    >
                                                    <label class="custom-control-label" for="customCheck2">Cancelar cirugía</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="text-right">
                                        	<br>
                                            <input style="width: 100%"  type="submit" name="editarCirugia" value="Editar Cirugía" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Consumo</h4>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th scope="col">Producto</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Costo</th>
                                                <th scope="col">Venta</th>
                                                <th scope="col">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($consumos as $consumo){ ?>
                                            <tr>
                                                <td><?php echo $consumo->nombre?></td>
                                                <td style="text-align: center;"><?php echo $consumo->cantidad ?></td>
                                                <td class="nowrap"><?php echo "$ ".number_format($consumo->precio,2); ?></td>
                                                <td class="nowrap"><?php echo "$ ".number_format($consumo->unitario,2); ?></td>
                                                <td>
                                                    <form method="post" action="../php/nuevo5.php" style="display: inline-block;">
                                                        <input type="hidden" name="id" value="<?php echo $cirugia->id ?>">
                                                        <input type="hidden" name="producto" value="<?php echo $consumo->id ?>">
                                                        <button type="submit" name="eliminarConsumo" class="btn btn-secondary btn-circle"  data-toggle="tooltip" title="Eliminar" style="background-color: transparent; border-color: transparent; color: #6c757d"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    	<div class="card">
                    		<div class="card-body">
                                <form role="form" name="registro" action="../php/nuevo5.php" method="post">
                                    <div class="form-body">
                                        <div class="form-group">
                                             <input type="hidden" name="id" value="<?php echo $cirugia->id?>">
                                             <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" required="" name="producto">
                                                <option selected="">Choose...</option>
                                                <?php foreach($productos as $producto){ ?>
                                                    <option value="<?php echo $producto->id?>"><?php echo $producto->nombre?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                          
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="number" class="form-control" placeholder="Cantidad" required="" name="cantidad">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" step="0.01" class="form-control" placeholder="Precio U. Costo" required="" name="precio">
                                                </div>
                                            </div>
                                    <div class="form-actions">
                                        <div class="text-right">
                                            <br>
                                            <input style="width: 100%"  type="submit" name="consumo" value="Agregar Producto" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="../dist/js/app-style-switcher.js"></script>
    <script src="../dist/js/feather.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <!-- themejs -->
    <!--Menu sidebar -->
    <script src="../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../dist/js/custom.min.js"></script>
    <!--This page plugins -->
    <script src="../assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../dist/js/pages/datatable/datatable-basic.init.js"></script>
</body>

</html>