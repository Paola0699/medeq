<?php ob_start(); ?>
<?php
	include_once "base_de_datos.php";
	$inicio=$_POST["inicio"];
	$fin = $_POST["fin"];
	$hospital = $_POST["hospital"];
    $sentencia = $base_de_datos->query("SELECT c.*, m.nombre as medico FROM cirugia c join medico m on c.medico = m.id where c.fecha >= '$inicio' AND c.fecha <= '$fin' AND c.hospital=$hospital" );
    $cirugias= $sentencia->fetchAll(PDO::FETCH_OBJ);
    $sent = $base_de_datos->prepare("SELECT * FROM hospital WHERE id=?;");
	$sent->execute([$hospital]);
	$hosp = $sent -> fetch(PDO::FETCH_OBJ);
    $i = 1;
    $totalfinal =0;
?>
<style type="text/css">
	table, th, td {
  		border: 1px solid black;
  		border-collapse: collapse;
  		font-size: 1em;
  		text-align: center;
	}
	@page{
		font-family: Arial, Helvetica, sans-serif;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<img src="logo_1.png" style="width: 30%; margin-left: 69%; margin-top: 2%">
	<h3>CORTE DE CIRUGÍAS DEL <?php echo date("d/m/Y", strtotime($inicio))?> AL <?php echo date("d/m/Y", strtotime($fin)). " , " . $hosp->nombre . " , " . $hosp->doctorencargado ?> </h3>
	 <table nowrap>
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">FECHA</th>
                <th scope="col">REM</th>
                <?php if($hospital != 1){?>
                    <th scope="col">FOLIO</th>
                <?php } ?>
                <th scope="col">HORA</th>
                <th scope="col">MÉDICO TRATANTE</th>
                <th scope="col">CX</th>
                <th scope="col">PACIENTE</th>
                <th scope="col">REGISTRO</th>
                <th scope="col">CONSUMO</th>
                <th scope="col">CLAVE SSNL</th>
                <th>SUBTOTAL</th>
                <th>IVA</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <?php foreach ($cirugias as $cirugia) {
        	if($cirugia->status != 1){
        		   include_once "base_de_datos.php";
		            $sentencia3 = $base_de_datos->prepare("SELECT count(*) as total FROM productocirugia WHERE cirugia=?;");
		            $sentencia3->execute([$cirugia->id]);
		            $filas = $sentencia3 -> fetch(PDO::FETCH_OBJ);
		            $sentencia2 = $base_de_datos->query("SELECT p.nombre as nombre, p.clave as clave, pc.cantidad as cantidad from productocirugia pc join producto p on pc.producto=p.id where pc.cirugia=$cirugia->id LIMIT 1");
		            $productos= $sentencia2 ->fetchAll(PDO::FETCH_OBJ); 
		            $f = $filas->total - 1;
		            $sentencia4 = $base_de_datos->query("SELECT p.nombre as nombre, p.clave as clave, pc.cantidad as cantidad from productocirugia pc join producto p on pc.producto=p.id where pc.cirugia=$cirugia->id ORDER BY p.id DESC LIMIT $f");
		            $productos2= $sentencia4->fetchAll(PDO::FETCH_OBJ);
		            $sentencia5 = $base_de_datos->prepare("SELECT SUM(productocirugia.cantidad*producto.preciounitario) as sub FROM productocirugia JOIN producto ON productocirugia.producto = producto.id WHERE productocirugia.cirugia=?;");
		            $sentencia5->execute([$cirugia->id]);
		            $subtotal = $sentencia5 -> fetch(PDO::FETCH_OBJ);   
        ?>
            <tr>
                <td rowspan="<?php echo $filas->total?>"><?php echo $i; $i++;?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo date("d/m/Y", strtotime($cirugia->fecha))  ?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->rem ?></td>
                <?php if($hospital!=1) { ?>
                    <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->folio ?></td>
                <?php }?>
                <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->hora  ?></td>
                <td rowspan="<?php echo $filas->total?>"><?php  echo $cirugia->medico  ?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->cx   ?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->paciente  ?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo $cirugia->registro ?></td>
                <?php foreach($productos as $producto) { ?>
                    <td nowrap="" ><?php echo $producto->cantidad . "-" . $producto->nombre ?></td>
                    <td nowrap=""><?php echo $producto->clave ?></td>
                <?php }?>
                <td rowspan="<?php echo $filas->total?>"><?php echo "$ ".number_format($subtotal->sub,2); $totalfinal += $subtotal->sub?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo "$ ".number_format($subtotal->sub *.16,2);?></td>
                <td rowspan="<?php echo $filas->total?>"><?php echo "$ ".number_format($subtotal->sub*1.16,2);?></td>
            </tr>
            <?php foreach($productos2 as $producto) { ?>
            	<tr>
            		<td nowrap=""><?php echo $producto->cantidad . "-" . $producto->nombre ?></td>
                    <td nowrap=""><?php echo $producto->clave ?></td>
            	</tr>
        	<?php }?>
        <?php }}?>

        	<tr>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
        		<td></td>
                <?php if($hospital != 1){ ?>
        		  <td></td>
                <?php } ?>
        		<td><b><?php echo "$ ".number_format($totalfinal,2);?></b></td>
        		<td><b><?php echo "$ ".number_format($totalfinal*.16,2);?></b></td>
        		<td><b><?php echo "$ ".number_format($totalfinal*1.16,2);?></b></td>
        	</tr>

    </table>
</body>
</html>

<?php
	require_once 'dompdf/autoload.inc.php';
	use Dompdf\Dompdf;
	$dompdf = new DOMPDF();
	$dompdf->load_html(ob_get_clean());
	$dompdf->setPaper('A3', 'landscape');
	$dompdf->render();
	$pdf = $dompdf->output();
	$filename = "CONCENTRADO_ALMACEN.pdf";
	file_put_contents($filename, $pdf);
	$dompdf->stream($filename);
?>
